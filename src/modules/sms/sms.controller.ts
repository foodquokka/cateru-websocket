import { Controller, Get, Post, Body } from '@nestjs/common';
import { MessagingGateway } from 'src/modules/messaging/messaging.gateway';

@Controller('/sms')
export class SmsController {
    constructor(private readonly messaging: MessagingGateway){}

    @Post('/send')
    sendSMS(@Body() body: any){
        console.log(body);
        this.messaging.sendMessage(body, "sms");
    }
}
