import { Module } from '@nestjs/common';
import { KitchenService } from './kitchen.service';
import { DbModule } from '../db/db.module';

@Module({
    imports:[DbModule],
    providers: [KitchenService],
    exports: [KitchenService]
})
export class KitchenModule {}
