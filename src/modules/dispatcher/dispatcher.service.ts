import { Injectable } from '@nestjs/common';
import { DbService } from '../db/db.service';

@Injectable()
export class DispatcherService {
    constructor(private readonly db: DbService) { }

    async getTableData(table:string) {
        var query = `SELECT 
        menus.menuID AS menuID, 
        kitchenrecords.id AS kitchen_id,
        kitchenrecords.order_id AS order_id,
        kitchenrecords.bundleid AS bundleid, 
        (SELECT bundles.name AS "name" FROM bundles WHERE kitchenrecords.bundleid = bundles.bundleid LIMIT 1) AS bundleName, 
        menus.name AS itemName, 
        kitchenrecords.orderQty AS ordered, 
        (SELECT date_ordered FROM order_details WHERE order_id = kitchenrecords.order_id LIMIT 1) AS date_ordered,
        kitchenrecords.status AS "status",
        (SELECT orders.tableno FROM orders WHERE orders.order_id = kitchenrecords.order_id LIMIT 1) AS table_number
        FROM kitchenrecords
        JOIN menus
        ON kitchenrecords.menuID = menus.menuID
        HAVING table_number = "${table}"`;

        var queryBundle = `SELECT 
        bundle_details.menuID AS menuID,
        kitchenrecords.id AS kitchen_id,
        kitchenrecords.order_id AS order_id,
        kitchenrecords.bundleid AS bundleid,
        (SELECT bundles.name AS "name" FROM bundles WHERE kitchenrecords.bundleid = bundles.bundleid LIMIT 1) AS bundleName, 
        menus.name AS itemName,
        kitchenrecords.orderQty AS ordered,  
        (SELECT date_ordered FROM order_details WHERE order_id = kitchenrecords.order_id LIMIT 1) AS date_ordered,
        kitchenrecords.status AS "status",
        (SELECT categories.categoryname FROM categories JOIN sub_categories ON sub_categories.categoryid = categories.categoryid WHERE sub_categories.subcatid = menus.subcatid) AS category,
        (bundle_details.qty * kitchenrecords.orderQty) as bundleItemQty,    
        (SELECT orders.tableno FROM orders WHERE orders.order_id = kitchenrecords.order_id LIMIT 1) AS table_number
        FROM kitchenrecords
        JOIN bundle_details
        ON kitchenrecords.bundleid = bundle_details.bundleid
        JOIN menus
        ON bundle_details.menuID = menus.menuID
        HAVING table_number = "${table}"`;
        var nonBundle = JSON.parse(JSON.stringify(await this.db.get(query)));
        nonBundle = this.parseNonBundleResult(nonBundle);
        var bundle = JSON.parse(JSON.stringify(await this.db.get(queryBundle)));
        bundle = this.parseBundleResult(bundle);
        // return bundle;
        var combinedResults = this.combineParsedResults(bundle,nonBundle);
        return this.groupByTable(combinedResults)[table];
    }

    async getTableStatus() {
        // var query = `SELECT 
        // (SELECT orders.tableno FROM orders WHERE orders.order_id = kitchenrecords.order_id LIMIT 1) AS tableno,
        // kitchenrecords.status
        // FROM kitchenrecords
        // ORDER BY tableno ASC;`;
        var query = `Select tables.tableno,kitchenrecords.status from orders 
        inner join tables 
        ON orders.tableno = tables.tableno
        inner join kitchenrecords
        on orders.order_id = kitchenrecords.order_id
        where tables.status = "Occupied"
        order by tableno ASC;`
        var tables = JSON.parse(JSON.stringify(await this.db.get(query)));
        return this.groupByTable2(tables);
    }

    

    groupByTable2(value:Array<any>){
        var result = {};
        for (var i=0; i<value.length; i++){
            if (result[value[i]["tableno"]]===null || result[value[i]["tableno"]]===undefined){
                result[value[i]["tableno"]] = [];
            }
            result[value[i]["tableno"]].push({status:value[i]["status"]});
        }
        return result;
    }


    groupByTable(value:Array<any>){
        var result = {};
        for (var i=0; i<value.length; i++){
            if (result[value[i]["table_number"]]===null || result[value[i]["table_number"]]===undefined){
                result[value[i]["table_number"]] = [];
            }
            result[value[i]["table_number"]].push(value[i])
        }
        return result;
    }


    compareDates(x:Date,y:Date) : number{
        return x.getTime()-y.getTime();
    }

    combineParsedResults(bundle: any, nonBundle:any){
        var result = [];
        //creating an array of unique orderIds from bundle and nonbundle
        var combinedKeys = Object.keys(bundle).concat(Object.keys(nonBundle));
        combinedKeys = Array.from(new Set(combinedKeys));
        //parsing
        for(let i = 0; i<combinedKeys.length; i++){
            // if (isNullOrUndefined(result[combinedKeys[i]]))
            // result[combinedKeys[i]] = [];
            //the order has both bundle and nonbundle
            if (!this.isNullOrUndefined(bundle[combinedKeys[i]]) && !this.isNullOrUndefined(nonBundle[combinedKeys[i]]))
            result = result.concat(nonBundle[combinedKeys[i]].concat(bundle[combinedKeys[i]]));
            //the order is purely bundles
            else if (!this.isNullOrUndefined(bundle[combinedKeys[i]]) && this.isNullOrUndefined(nonBundle[combinedKeys[i]]))
            result = result.concat(bundle[combinedKeys[i]]);
            //the order is purely nonbundle
            else 
            result = result.concat(nonBundle[combinedKeys[i]]);
        }
        return result;
    }
    parseNonBundleResult(value: Array<any>) {
        let resultArr = {};
        for (let x = 0; x < value.length; x++) {
            if (this.isNullOrUndefined(resultArr[value[x]['order_id']]))
                resultArr[value[x]["order_id"]] = [];
            delete value[x]["category"];
            value[x]["details"] = null;
            resultArr[value[x]["order_id"]].push(JSON.parse(JSON.stringify(value[x])));
        }
        return resultArr;
    }
    parseBundleResult(value: Array<any>) {
        let resultArr = {};
        let tmp = [];
        for (let x = 0; x < value.length; x++) {
            // console.log(x);
            if (this.isNullOrUndefined(resultArr[value[x]["order_id"]])) {
                resultArr[value[x]["order_id"]] = {};
            }
            if (this.isNullOrUndefined(resultArr[value[x]["order_id"]][value[x]["bundleid"]])) {
                resultArr[value[x]["order_id"]][value[x]["bundleid"]] = JSON.parse(JSON.stringify(value[x]));
                resultArr[value[x]["order_id"]][value[x]["bundleid"]]["details"] = [this.parseEntryForDetails(value[x])];
                resultArr[value[x]["order_id"]][value[x]["bundleid"]]["menuID"] = null;
                resultArr[value[x]["order_id"]][value[x]["bundleid"]]["itemName"] = null;
                delete resultArr[value[x]["order_id"]][value[x]["bundleid"]]["bundleItemQty"]
                delete resultArr[value[x]["order_id"]][value[x]["bundleid"]]["category"];
            }
            else resultArr[value[x]["order_id"]][value[x]["bundleid"]]["details"].push(this.parseEntryForDetails(value[x]));
        }
        // return resultArr;
        // console.log(resultArr);
        let orderIds = Object.keys(resultArr);
        // console.log(orderIds);
        for (let y = 0; y < orderIds.length; y++) {
            tmp=[];
            let orderItems = Object.keys(resultArr[orderIds[y]]);
            // console.log(orderItems);
            for (let z = 0; z < orderItems.length; z++) {
                // console.log(resultArr[orderIds[y]][orderItems[z]]);
                tmp.push(resultArr[orderIds[y]][orderItems[z]]);
            }
            resultArr[orderIds[y]] = tmp;
        }
        return resultArr;
    }
    parseEntryForDetails(value: any) {
        return {
            menuID: value["menuID"], itemName: value["itemName"], qty: value["bundleItemQty"]
        };
    }

    isNullOrUndefined(val:any){
        return val===null||val===undefined;
    }
}
