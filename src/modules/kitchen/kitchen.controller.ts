import { Controller, Get, Post, Body } from '@nestjs/common';
import { KitchenService } from './kitchen.service';

@Controller('/kitchen')
export class KitchenController {

    constructor(private readonly kitchenService: KitchenService){}

    @Get('/getOrders/waiting')
    async waiting(){
        console.log("waiting "+Date.now());
         return await this.kitchenService.getOrders("waiting", false);
    }

    @Get('/getOrders/preparing')
    async preparing(){
        console.log("preparing "+Date.now());
         return await this.kitchenService.getOrders("preparing", false);
    }
    @Get('/getOrders/ready')
    async ready(){
        console.log("ready "+Date.now());
         return await this.kitchenService.getOrders("ready", false);
    }

    @Get('/bar/getOrders/waiting')
    async waiting_drinks(){
        console.log("bar_waiting "+Date.now());
        var result = await this.kitchenService.getOrders("waiting", true);
        console.log(result);
        return result;
    }

    @Get('/bar/getOrders/preparing')
    async preparing_drinks(){
        console.log("bar_preparing "+Date.now());
        return await this.kitchenService.getOrders("preparing", true);
    }
    @Get('/bar/getOrders/ready')
    async ready_drinks(){
        console.log("bar_ready "+Date.now());
        return await this.kitchenService.getOrders("ready", true);
    }

}
