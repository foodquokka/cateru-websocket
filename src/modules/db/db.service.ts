import { Injectable } from '@nestjs/common';
import * as mysql from 'mysql';

@Injectable()
export class DbService {
    pool: any;
    queryResult: any;

    constructor() {
        this.pool = mysql.createPool({
            connectionLimit: 10,
            host: '127.0.0.1',
            user: 'root',
            password: 'cateru123',
            database: 'cater'
        });
    }

    async get(query: string) {
        return new Promise((resolve) => {
            this.pool.query(query, (error, results) => {
                resolve(results);
            });
        });
    }

}
