import { Test, TestingModule } from '@nestjs/testing';
import { RecService } from './rec.service';

describe('RecService', () => {
  let service: RecService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RecService],
    }).compile();

    service = module.get<RecService>(RecService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
