import { Test, TestingModule } from '@nestjs/testing';
import { RecController } from './rec.controller';

describe('RecController', () => {
  let controller: RecController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RecController],
    }).compile();

    controller = module.get<RecController>(RecController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
