import { HttpModule, HttpService, Module } from '@nestjs/common';
import { DbModule } from '../db/db.module';
import { DbService } from '../db/db.service';
import { RecController } from './rec.controller';
import { RecService } from './rec.service';

@Module({
  imports: [DbModule, HttpModule],
  controllers: [RecController],
  providers: [RecService, DbService]
})
export class RecModule {}
