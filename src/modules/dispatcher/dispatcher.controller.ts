import { Controller, Get, Post, Body, Param } from '@nestjs/common';
import { KitchenService } from '../kitchen/kitchen.service';
import { DispatcherService } from './dispatcher.service';

@Controller('/dispatcher')
export class DispatcherController {
    constructor(private readonly kitchenService: KitchenService, private readonly dispatcherService: DispatcherService){}

    @Get('/getTableData/:table')
    async tables(@Param('table') table: string ){
        console.log("TableData "+Date.now());
        return await this.dispatcherService.getTableData(table);
    }
    @Get('/getTableStatus')
    async tablesStatus(){
        console.log("TableStatus "+Date.now());
        return await this.dispatcherService.getTableStatus();
    }
}
