import { HttpService, Injectable } from '@nestjs/common';
import { DbService } from '../db/db.service';
import { Apriori, Itemset, IAprioriResults } from 'node-apriori';
@Injectable()
export class RecService {
    constructor(private db: DbService, private http: HttpService) {
    }
    private result = [
        {
            "menuID": 3,
            "order_id": 1
        },
        {
            "menuID": 5,
            "order_id": 1
        },
        {
            "menuID": 8,
            "order_id": 1
        },
        {
            "menuID": 9,
            "order_id": 1
        },
        {
            "menuID": 10,
            "order_id": 1
        },
        {
            "menuID": 13,
            "order_id": 1
        },
        {
            "menuID": 17,
            "order_id": 1
        },
        {
            "menuID": 18,
            "order_id": 1
        },
        {
            "menuID": 19,
            "order_id": 1
        },
        {
            "menuID": 24,
            "order_id": 1
        },
        {
            "menuID": 2,
            "order_id": 2
        },
        {
            "menuID": 3,
            "order_id": 2
        },
        {
            "menuID": 8,
            "order_id": 2
        },
        {
            "menuID": 9,
            "order_id": 2
        },
        {
            "menuID": 11,
            "order_id": 2
        },
        {
            "menuID": 12,
            "order_id": 2
        },
        {
            "menuID": 14,
            "order_id": 2
        },
        {
            "menuID": 15,
            "order_id": 2
        },
        {
            "menuID": 16,
            "order_id": 2
        },
        {
            "menuID": 18,
            "order_id": 2
        },
        {
            "menuID": 20,
            "order_id": 2
        },
        {
            "menuID": 21,
            "order_id": 2
        },
        {
            "menuID": 22,
            "order_id": 2
        },
        {
            "menuID": 23,
            "order_id": 2
        },
        {
            "menuID": 24,
            "order_id": 2
        },
        {
            "menuID": 2,
            "order_id": 3
        },
        {
            "menuID": 3,
            "order_id": 3
        },
        {
            "menuID": 5,
            "order_id": 3
        },
        {
            "menuID": 8,
            "order_id": 3
        },
        {
            "menuID": 9,
            "order_id": 3
        },
        {
            "menuID": 11,
            "order_id": 3
        },
        {
            "menuID": 12,
            "order_id": 3
        },
        {
            "menuID": 15,
            "order_id": 3
        },
        {
            "menuID": 16,
            "order_id": 3
        },
        {
            "menuID": 18,
            "order_id": 3
        },
        {
            "menuID": 19,
            "order_id": 3
        },
        {
            "menuID": 21,
            "order_id": 3
        },
        {
            "menuID": 22,
            "order_id": 3
        },
        {
            "menuID": 23,
            "order_id": 3
        },
        {
            "menuID": 2,
            "order_id": 4
        },
        {
            "menuID": 5,
            "order_id": 4
        },
        {
            "menuID": 8,
            "order_id": 4
        },
        {
            "menuID": 10,
            "order_id": 4
        },
        {
            "menuID": 13,
            "order_id": 4
        },
        {
            "menuID": 14,
            "order_id": 4
        },
        {
            "menuID": 15,
            "order_id": 4
        },
        {
            "menuID": 17,
            "order_id": 4
        },
        {
            "menuID": 18,
            "order_id": 4
        },
        {
            "menuID": 20,
            "order_id": 4
        },
        {
            "menuID": 22,
            "order_id": 4
        },
        {
            "menuID": 24,
            "order_id": 4
        },
        {
            "menuID": 3,
            "order_id": 5
        },
        {
            "menuID": 5,
            "order_id": 5
        },
        {
            "menuID": 8,
            "order_id": 5
        },
        {
            "menuID": 9,
            "order_id": 5
        },
        {
            "menuID": 10,
            "order_id": 5
        },
        {
            "menuID": 12,
            "order_id": 5
        },
        {
            "menuID": 13,
            "order_id": 5
        },
        {
            "menuID": 14,
            "order_id": 5
        },
        {
            "menuID": 17,
            "order_id": 5
        },
        {
            "menuID": 18,
            "order_id": 5
        },
        {
            "menuID": 20,
            "order_id": 5
        },
        {
            "menuID": 21,
            "order_id": 5
        },
        {
            "menuID": 22,
            "order_id": 5
        },
        {
            "menuID": 2,
            "order_id": 6
        },
        {
            "menuID": 3,
            "order_id": 6
        },
        {
            "menuID": 8,
            "order_id": 6
        },
        {
            "menuID": 9,
            "order_id": 6
        },
        {
            "menuID": 12,
            "order_id": 6
        },
        {
            "menuID": 14,
            "order_id": 6
        },
        {
            "menuID": 15,
            "order_id": 6
        },
        {
            "menuID": 16,
            "order_id": 6
        },
        {
            "menuID": 18,
            "order_id": 6
        },
        {
            "menuID": 19,
            "order_id": 6
        },
        {
            "menuID": 21,
            "order_id": 6
        },
        {
            "menuID": 23,
            "order_id": 6
        },
        {
            "menuID": 24,
            "order_id": 6
        },
        {
            "menuID": 2,
            "order_id": 7
        },
        {
            "menuID": 3,
            "order_id": 7
        },
        {
            "menuID": 5,
            "order_id": 7
        },
        {
            "menuID": 8,
            "order_id": 7
        },
        {
            "menuID": 9,
            "order_id": 7
        },
        {
            "menuID": 11,
            "order_id": 7
        },
        {
            "menuID": 12,
            "order_id": 7
        },
        {
            "menuID": 14,
            "order_id": 7
        },
        {
            "menuID": 16,
            "order_id": 7
        },
        {
            "menuID": 17,
            "order_id": 7
        },
        {
            "menuID": 20,
            "order_id": 7
        },
        {
            "menuID": 21,
            "order_id": 7
        },
        {
            "menuID": 23,
            "order_id": 7
        },
        {
            "menuID": 2,
            "order_id": 8
        },
        {
            "menuID": 3,
            "order_id": 8
        },
        {
            "menuID": 5,
            "order_id": 8
        },
        {
            "menuID": 8,
            "order_id": 8
        },
        {
            "menuID": 14,
            "order_id": 8
        },
        {
            "menuID": 15,
            "order_id": 8
        },
        {
            "menuID": 16,
            "order_id": 8
        },
        {
            "menuID": 18,
            "order_id": 8
        },
        {
            "menuID": 19,
            "order_id": 8
        },
        {
            "menuID": 20,
            "order_id": 8
        },
        {
            "menuID": 21,
            "order_id": 8
        },
        {
            "menuID": 22,
            "order_id": 8
        },
        {
            "menuID": 23,
            "order_id": 8
        },
        {
            "menuID": 2,
            "order_id": 9
        },
        {
            "menuID": 3,
            "order_id": 9
        },
        {
            "menuID": 5,
            "order_id": 9
        },
        {
            "menuID": 10,
            "order_id": 9
        },
        {
            "menuID": 14,
            "order_id": 9
        },
        {
            "menuID": 16,
            "order_id": 9
        },
        {
            "menuID": 17,
            "order_id": 9
        },
        {
            "menuID": 19,
            "order_id": 9
        },
        {
            "menuID": 20,
            "order_id": 9
        },
        {
            "menuID": 21,
            "order_id": 9
        },
        {
            "menuID": 22,
            "order_id": 9
        },
        {
            "menuID": 2,
            "order_id": 10
        },
        {
            "menuID": 3,
            "order_id": 10
        },
        {
            "menuID": 5,
            "order_id": 10
        },
        {
            "menuID": 8,
            "order_id": 10
        },
        {
            "menuID": 10,
            "order_id": 10
        },
        {
            "menuID": 11,
            "order_id": 10
        },
        {
            "menuID": 12,
            "order_id": 10
        },
        {
            "menuID": 14,
            "order_id": 10
        },
        {
            "menuID": 16,
            "order_id": 10
        },
        {
            "menuID": 17,
            "order_id": 10
        },
        {
            "menuID": 18,
            "order_id": 10
        },
        {
            "menuID": 20,
            "order_id": 10
        },
        {
            "menuID": 21,
            "order_id": 10
        },
        {
            "menuID": 22,
            "order_id": 10
        },
        {
            "menuID": 24,
            "order_id": 10
        },
        {
            "menuID": 2,
            "order_id": 11
        },
        {
            "menuID": 3,
            "order_id": 11
        },
        {
            "menuID": 5,
            "order_id": 11
        },
        {
            "menuID": 8,
            "order_id": 11
        },
        {
            "menuID": 10,
            "order_id": 11
        },
        {
            "menuID": 11,
            "order_id": 11
        },
        {
            "menuID": 13,
            "order_id": 11
        },
        {
            "menuID": 14,
            "order_id": 11
        },
        {
            "menuID": 16,
            "order_id": 11
        },
        {
            "menuID": 20,
            "order_id": 11
        },
        {
            "menuID": 21,
            "order_id": 11
        },
        {
            "menuID": 24,
            "order_id": 11
        },
        {
            "menuID": 2,
            "order_id": 12
        },
        {
            "menuID": 3,
            "order_id": 12
        },
        {
            "menuID": 5,
            "order_id": 12
        },
        {
            "menuID": 10,
            "order_id": 12
        },
        {
            "menuID": 12,
            "order_id": 12
        },
        {
            "menuID": 13,
            "order_id": 12
        },
        {
            "menuID": 14,
            "order_id": 12
        },
        {
            "menuID": 16,
            "order_id": 12
        },
        {
            "menuID": 18,
            "order_id": 12
        },
        {
            "menuID": 20,
            "order_id": 12
        },
        {
            "menuID": 21,
            "order_id": 12
        },
        {
            "menuID": 22,
            "order_id": 12
        },
        {
            "menuID": 2,
            "order_id": 13
        },
        {
            "menuID": 3,
            "order_id": 13
        },
        {
            "menuID": 5,
            "order_id": 13
        },
        {
            "menuID": 8,
            "order_id": 13
        },
        {
            "menuID": 9,
            "order_id": 13
        },
        {
            "menuID": 10,
            "order_id": 13
        },
        {
            "menuID": 11,
            "order_id": 13
        },
        {
            "menuID": 12,
            "order_id": 13
        },
        {
            "menuID": 14,
            "order_id": 13
        },
        {
            "menuID": 16,
            "order_id": 13
        },
        {
            "menuID": 17,
            "order_id": 13
        },
        {
            "menuID": 18,
            "order_id": 13
        },
        {
            "menuID": 20,
            "order_id": 13
        },
        {
            "menuID": 21,
            "order_id": 13
        },
        {
            "menuID": 22,
            "order_id": 13
        },
        {
            "menuID": 2,
            "order_id": 14
        },
        {
            "menuID": 3,
            "order_id": 14
        },
        {
            "menuID": 5,
            "order_id": 14
        },
        {
            "menuID": 10,
            "order_id": 14
        },
        {
            "menuID": 12,
            "order_id": 14
        },
        {
            "menuID": 13,
            "order_id": 14
        },
        {
            "menuID": 14,
            "order_id": 14
        },
        {
            "menuID": 16,
            "order_id": 14
        },
        {
            "menuID": 17,
            "order_id": 14
        },
        {
            "menuID": 18,
            "order_id": 14
        },
        {
            "menuID": 20,
            "order_id": 14
        },
        {
            "menuID": 21,
            "order_id": 14
        },
        {
            "menuID": 22,
            "order_id": 14
        },
        {
            "menuID": 2,
            "order_id": 15
        },
        {
            "menuID": 3,
            "order_id": 15
        },
        {
            "menuID": 5,
            "order_id": 15
        },
        {
            "menuID": 8,
            "order_id": 15
        },
        {
            "menuID": 10,
            "order_id": 15
        },
        {
            "menuID": 11,
            "order_id": 15
        },
        {
            "menuID": 12,
            "order_id": 15
        },
        {
            "menuID": 14,
            "order_id": 15
        },
        {
            "menuID": 17,
            "order_id": 15
        },
        {
            "menuID": 19,
            "order_id": 15
        },
        {
            "menuID": 20,
            "order_id": 15
        },
        {
            "menuID": 22,
            "order_id": 15
        },
        {
            "menuID": 24,
            "order_id": 15
        },
        {
            "menuID": 2,
            "order_id": 16
        },
        {
            "menuID": 3,
            "order_id": 16
        },
        {
            "menuID": 5,
            "order_id": 16
        },
        {
            "menuID": 8,
            "order_id": 16
        },
        {
            "menuID": 9,
            "order_id": 16
        },
        {
            "menuID": 10,
            "order_id": 16
        },
        {
            "menuID": 12,
            "order_id": 16
        },
        {
            "menuID": 13,
            "order_id": 16
        },
        {
            "menuID": 14,
            "order_id": 16
        },
        {
            "menuID": 16,
            "order_id": 16
        },
        {
            "menuID": 17,
            "order_id": 16
        },
        {
            "menuID": 20,
            "order_id": 16
        },
        {
            "menuID": 21,
            "order_id": 16
        },
        {
            "menuID": 22,
            "order_id": 16
        },
        {
            "menuID": 24,
            "order_id": 16
        },
        {
            "menuID": 2,
            "order_id": 17
        },
        {
            "menuID": 3,
            "order_id": 17
        },
        {
            "menuID": 5,
            "order_id": 17
        },
        {
            "menuID": 8,
            "order_id": 17
        },
        {
            "menuID": 10,
            "order_id": 17
        },
        {
            "menuID": 11,
            "order_id": 17
        },
        {
            "menuID": 12,
            "order_id": 17
        },
        {
            "menuID": 14,
            "order_id": 17
        },
        {
            "menuID": 16,
            "order_id": 17
        },
        {
            "menuID": 17,
            "order_id": 17
        },
        {
            "menuID": 18,
            "order_id": 17
        },
        {
            "menuID": 20,
            "order_id": 17
        },
        {
            "menuID": 21,
            "order_id": 17
        },
        {
            "menuID": 22,
            "order_id": 17
        },
        {
            "menuID": 23,
            "order_id": 17
        },
        {
            "menuID": 2,
            "order_id": 18
        },
        {
            "menuID": 3,
            "order_id": 18
        },
        {
            "menuID": 5,
            "order_id": 18
        },
        {
            "menuID": 8,
            "order_id": 18
        },
        {
            "menuID": 9,
            "order_id": 18
        },
        {
            "menuID": 12,
            "order_id": 18
        },
        {
            "menuID": 13,
            "order_id": 18
        },
        {
            "menuID": 14,
            "order_id": 18
        },
        {
            "menuID": 17,
            "order_id": 18
        },
        {
            "menuID": 18,
            "order_id": 18
        },
        {
            "menuID": 20,
            "order_id": 18
        },
        {
            "menuID": 21,
            "order_id": 18
        },
        {
            "menuID": 22,
            "order_id": 18
        },
        {
            "menuID": 2,
            "order_id": 19
        },
        {
            "menuID": 3,
            "order_id": 19
        },
        {
            "menuID": 5,
            "order_id": 19
        },
        {
            "menuID": 8,
            "order_id": 19
        },
        {
            "menuID": 10,
            "order_id": 19
        },
        {
            "menuID": 11,
            "order_id": 19
        },
        {
            "menuID": 14,
            "order_id": 19
        },
        {
            "menuID": 16,
            "order_id": 19
        },
        {
            "menuID": 17,
            "order_id": 19
        },
        {
            "menuID": 19,
            "order_id": 19
        },
        {
            "menuID": 20,
            "order_id": 19
        },
        {
            "menuID": 21,
            "order_id": 19
        },
        {
            "menuID": 22,
            "order_id": 19
        },
        {
            "menuID": 24,
            "order_id": 19
        },
        {
            "menuID": 2,
            "order_id": 20
        },
        {
            "menuID": 3,
            "order_id": 20
        },
        {
            "menuID": 5,
            "order_id": 20
        },
        {
            "menuID": 8,
            "order_id": 20
        },
        {
            "menuID": 9,
            "order_id": 20
        },
        {
            "menuID": 10,
            "order_id": 20
        },
        {
            "menuID": 12,
            "order_id": 20
        },
        {
            "menuID": 14,
            "order_id": 20
        },
        {
            "menuID": 16,
            "order_id": 20
        },
        {
            "menuID": 17,
            "order_id": 20
        },
        {
            "menuID": 18,
            "order_id": 20
        },
        {
            "menuID": 20,
            "order_id": 20
        },
        {
            "menuID": 21,
            "order_id": 20
        },
        {
            "menuID": 22,
            "order_id": 20
        },
        {
            "menuID": 2,
            "order_id": 21
        },
        {
            "menuID": 3,
            "order_id": 21
        },
        {
            "menuID": 5,
            "order_id": 21
        },
        {
            "menuID": 8,
            "order_id": 21
        },
        {
            "menuID": 10,
            "order_id": 21
        },
        {
            "menuID": 11,
            "order_id": 21
        },
        {
            "menuID": 13,
            "order_id": 21
        },
        {
            "menuID": 14,
            "order_id": 21
        },
        {
            "menuID": 16,
            "order_id": 21
        },
        {
            "menuID": 18,
            "order_id": 21
        },
        {
            "menuID": 20,
            "order_id": 21
        },
        {
            "menuID": 21,
            "order_id": 21
        },
        {
            "menuID": 22,
            "order_id": 21
        },
        {
            "menuID": 24,
            "order_id": 21
        },
        {
            "menuID": 2,
            "order_id": 22
        },
        {
            "menuID": 3,
            "order_id": 22
        },
        {
            "menuID": 5,
            "order_id": 22
        },
        {
            "menuID": 8,
            "order_id": 22
        },
        {
            "menuID": 12,
            "order_id": 22
        },
        {
            "menuID": 14,
            "order_id": 22
        },
        {
            "menuID": 17,
            "order_id": 22
        },
        {
            "menuID": 20,
            "order_id": 22
        },
        {
            "menuID": 21,
            "order_id": 22
        },
        {
            "menuID": 22,
            "order_id": 22
        },
        {
            "menuID": 23,
            "order_id": 22
        },
        {
            "menuID": 2,
            "order_id": 23
        },
        {
            "menuID": 3,
            "order_id": 23
        },
        {
            "menuID": 5,
            "order_id": 23
        },
        {
            "menuID": 8,
            "order_id": 23
        },
        {
            "menuID": 10,
            "order_id": 23
        },
        {
            "menuID": 14,
            "order_id": 23
        },
        {
            "menuID": 16,
            "order_id": 23
        },
        {
            "menuID": 17,
            "order_id": 23
        },
        {
            "menuID": 18,
            "order_id": 23
        },
        {
            "menuID": 20,
            "order_id": 23
        },
        {
            "menuID": 21,
            "order_id": 23
        },
        {
            "menuID": 22,
            "order_id": 23
        },
        {
            "menuID": 2,
            "order_id": 24
        },
        {
            "menuID": 3,
            "order_id": 24
        },
        {
            "menuID": 5,
            "order_id": 24
        },
        {
            "menuID": 8,
            "order_id": 24
        },
        {
            "menuID": 12,
            "order_id": 24
        },
        {
            "menuID": 14,
            "order_id": 24
        },
        {
            "menuID": 16,
            "order_id": 24
        },
        {
            "menuID": 20,
            "order_id": 24
        },
        {
            "menuID": 21,
            "order_id": 24
        },
        {
            "menuID": 22,
            "order_id": 24
        },
        {
            "menuID": 24,
            "order_id": 24
        },
        {
            "menuID": 2,
            "order_id": 25
        },
        {
            "menuID": 3,
            "order_id": 25
        },
        {
            "menuID": 5,
            "order_id": 25
        },
        {
            "menuID": 8,
            "order_id": 25
        },
        {
            "menuID": 10,
            "order_id": 25
        },
        {
            "menuID": 11,
            "order_id": 25
        },
        {
            "menuID": 13,
            "order_id": 25
        },
        {
            "menuID": 14,
            "order_id": 25
        },
        {
            "menuID": 16,
            "order_id": 25
        },
        {
            "menuID": 17,
            "order_id": 25
        },
        {
            "menuID": 18,
            "order_id": 25
        },
        {
            "menuID": 20,
            "order_id": 25
        },
        {
            "menuID": 21,
            "order_id": 25
        },
        {
            "menuID": 22,
            "order_id": 25
        },
        {
            "menuID": 2,
            "order_id": 26
        },
        {
            "menuID": 3,
            "order_id": 26
        },
        {
            "menuID": 5,
            "order_id": 26
        },
        {
            "menuID": 8,
            "order_id": 26
        },
        {
            "menuID": 11,
            "order_id": 26
        },
        {
            "menuID": 12,
            "order_id": 26
        },
        {
            "menuID": 14,
            "order_id": 26
        },
        {
            "menuID": 16,
            "order_id": 26
        },
        {
            "menuID": 17,
            "order_id": 26
        },
        {
            "menuID": 20,
            "order_id": 26
        },
        {
            "menuID": 21,
            "order_id": 26
        },
        {
            "menuID": 22,
            "order_id": 26
        },
        {
            "menuID": 24,
            "order_id": 26
        },
        {
            "menuID": 2,
            "order_id": 27
        },
        {
            "menuID": 3,
            "order_id": 27
        },
        {
            "menuID": 5,
            "order_id": 27
        },
        {
            "menuID": 8,
            "order_id": 27
        },
        {
            "menuID": 10,
            "order_id": 27
        },
        {
            "menuID": 12,
            "order_id": 27
        },
        {
            "menuID": 16,
            "order_id": 27
        },
        {
            "menuID": 17,
            "order_id": 27
        },
        {
            "menuID": 18,
            "order_id": 27
        },
        {
            "menuID": 20,
            "order_id": 27
        },
        {
            "menuID": 21,
            "order_id": 27
        },
        {
            "menuID": 22,
            "order_id": 27
        },
        {
            "menuID": 2,
            "order_id": 28
        },
        {
            "menuID": 3,
            "order_id": 28
        },
        {
            "menuID": 9,
            "order_id": 28
        },
        {
            "menuID": 11,
            "order_id": 28
        },
        {
            "menuID": 12,
            "order_id": 28
        },
        {
            "menuID": 16,
            "order_id": 28
        },
        {
            "menuID": 17,
            "order_id": 28
        },
        {
            "menuID": 19,
            "order_id": 28
        },
        {
            "menuID": 20,
            "order_id": 28
        },
        {
            "menuID": 21,
            "order_id": 28
        },
        {
            "menuID": 22,
            "order_id": 28
        },
        {
            "menuID": 23,
            "order_id": 28
        },
        {
            "menuID": 2,
            "order_id": 29
        },
        {
            "menuID": 3,
            "order_id": 29
        },
        {
            "menuID": 5,
            "order_id": 29
        },
        {
            "menuID": 8,
            "order_id": 29
        },
        {
            "menuID": 9,
            "order_id": 29
        },
        {
            "menuID": 10,
            "order_id": 29
        },
        {
            "menuID": 12,
            "order_id": 29
        },
        {
            "menuID": 13,
            "order_id": 29
        },
        {
            "menuID": 17,
            "order_id": 29
        },
        {
            "menuID": 18,
            "order_id": 29
        },
        {
            "menuID": 20,
            "order_id": 29
        },
        {
            "menuID": 21,
            "order_id": 29
        },
        {
            "menuID": 22,
            "order_id": 29
        },
        {
            "menuID": 24,
            "order_id": 29
        },
        {
            "menuID": 2,
            "order_id": 30
        },
        {
            "menuID": 3,
            "order_id": 30
        },
        {
            "menuID": 5,
            "order_id": 30
        },
        {
            "menuID": 8,
            "order_id": 30
        },
        {
            "menuID": 10,
            "order_id": 30
        },
        {
            "menuID": 11,
            "order_id": 30
        },
        {
            "menuID": 12,
            "order_id": 30
        },
        {
            "menuID": 14,
            "order_id": 30
        },
        {
            "menuID": 17,
            "order_id": 30
        },
        {
            "menuID": 18,
            "order_id": 30
        },
        {
            "menuID": 20,
            "order_id": 30
        },
        {
            "menuID": 21,
            "order_id": 30
        },
        {
            "menuID": 22,
            "order_id": 30
        },
        {
            "menuID": 23,
            "order_id": 30
        },
        {
            "menuID": 24,
            "order_id": 30
        },
        {
            "menuID": 2,
            "order_id": 53
        },
        {
            "menuID": 3,
            "order_id": 53
        },
        {
            "menuID": null,
            "order_id": 53
        },
        {
            "menuID": 3,
            "order_id": 53
        },
        {
            "menuID": 9,
            "order_id": 53
        },
        {
            "menuID": 12,
            "order_id": 53
        },
        {
            "menuID": 8,
            "order_id": 53
        },
        {
            "menuID": 15,
            "order_id": 53
        },
        {
            "menuID": 10,
            "order_id": 53
        },
        {
            "menuID": 8,
            "order_id": 53
        },
        {
            "menuID": 5,
            "order_id": 53
        }
    ];

    async getRecommendation(input: string[], orderId: number) {
        var searchTerm: number[] = [];
        //convert the string to int 
        input.forEach(sN => {
            searchTerm.push(parseInt(sN));
        });
        if (orderId!=undefined) {
            var cart: any = await this.db.get(`SELECT menuID FROM cater.carts where order_id = ${orderId};`);
            if (cart.length!=0){
                cart.forEach(cI => {
                    searchTerm.push(cI.menuID);
                });
            }
        }
        searchTerm = Array.from(new Set(searchTerm));
        console.log("SearchTerm");
        console.log(searchTerm)
        var searchResults: number[][] = [];
        var parsed: any = await this.getAprioriData();
        while (searchResults.length == 0) {
            //sort the search terms in ascending order
            searchTerm.sort((a, b) => {
                if (a < b) return -1;
                else if (a > b) return 1;
                else return 0;
            }
            );
            parsed.forEach((set: number[]) => {
                searchTerm.forEach(item => {
                    // console.log(item, set);
                    if (set.includes(item)) searchResults.push(set);
                });
            });
            //if there are no results for the search terms, add a dither value
            if (searchResults.length == 0) {
                console.log("No result, adding dither value:");
                var dither = this.getRandomInt(18);
                console.log(dither);
                searchTerm.push(dither);
            }
        }
        //frequency of result search
        //get the frequencies of each element as they appear 
        var frequencies = {};
        searchResults.forEach((set: number[]) => {
            set.forEach((item: number) => {
                if (!searchTerm.includes(item)) {
                    if (frequencies[item.toString()] == undefined) frequencies[item.toString()] = 1;
                    else frequencies[item.toString()]++;
                }
            })
        });
        var resultant = [];
        //convert the frequencies to percentage of appearance based on n sets of search result. eg if 3 appears 3 times in a result of 6 sets it has a frequency of 3/6
        Object.entries(frequencies).forEach((v: any) => {
            // console.log(v);
            resultant.push([parseInt(v[0]), v[1] / searchResults.length]);
        });
        //sort based on descending frequency
        resultant.sort((a, b) => {
            if (a[1] < b[1]) return 1;
            else if (a[1] > b[1]) return -1;
            else return 0
        });
        console.log("results found");
        console.log(resultant.length);
        console.log("Frequencies");
        console.log(resultant);
        //get random n results
        if (resultant.length > 5) {
            resultant = this.getRandomResults(5, resultant);
            //sort based on descending frequency
            resultant.sort((a, b) => {
                if (a[1] < b[1]) return 1;
                else if (a[1] > b[1]) return -1;
                else return 0
            });
            console.log("After random");
            console.log(resultant);
        }
        var batch: Promise<any>[] = [];
        resultant.forEach((r) => {
            batch.push(this.getItemData(r[0]));
        });
        var payload = { menu: await Promise.all(batch) };
        // console.log(payload);
        return payload;
    }

    getRandomInt(max: number) {
        return Math.floor(Math.random() * Math.floor(max));
    }

    getRandomResults(k: number, input: any[]) {
        var result = [];
        var output = [];
        while (result.length < k) {
            result.push(this.getRandomInt(input.length));
            result = Array.from(new Set(result));
        }
        console.log(result);
        result.forEach((i: number) => {
            output.push(input[i]);
        });
        return output;
    }
    async getItemData(menuID: number) {
        var result = await this.http
            .get(`https://cateru.zdgph.tech/api/menu/getMenuByID/${menuID}`)
            .toPromise();
        return result["data"]["data"][0];
    }

    async getAprioriData() {
        var parsed = {};
        var result: any = await this.db.get("SELECT menuID, groupNumber FROM cater.apriori;");
        result.forEach(item => {
            if (item["menuID"] == null) return;
            if (parsed[item["groupNumber"]] == undefined) parsed[item["groupNumber"]] = [];
            parsed[item["groupNumber"]].push(item["menuID"]);
        });
        return Object.values(parsed);
    }

    getFrequentItemSets(transactions: number[][], threshold: number) {
        console.log(threshold / 10);
        return new Promise((resolve, reject) => {
            let apriori: Apriori<number> = new Apriori<number>(threshold / 10);
            apriori.exec(transactions)
                .then((result: IAprioriResults<number>) => {
                    // Returns both the collection of frequent itemsets and execution time in millisecond.
                    let frequentItemsets: Itemset<number>[] = result.itemsets;
                    if (result) return resolve(frequentItemsets);
                });
        });
    }
}
