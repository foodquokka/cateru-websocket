import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MessagingModule } from './modules/messaging/messaging.module';
import { SmsController } from './modules/sms/sms.controller';
import { SmsModule } from './modules/sms/sms.module';
import { EventController } from './modules/event/event.controller';
import { EventModule } from './modules/event/event.module';
import { KitchenController } from './modules/kitchen/kitchen.controller';
import { KitchenService } from './modules/kitchen/kitchen.service';
import { KitchenModule } from './modules/kitchen/kitchen.module';
import { DbModule } from './modules/db/db.module';
import { DispatcherModule } from './modules/dispatcher/dispatcher.module';
import { ServicesModule } from './services/services/services.module';
import { RecommenderService } from './services/recommender/recommender.service';
import { RecModule } from './modules/rec/rec.module';

@Module({
  imports: [MessagingModule, SmsModule, EventModule, KitchenModule, DbModule, DispatcherModule, ServicesModule, RecModule],
  controllers: [AppController, SmsController, EventController, KitchenController],
  providers: [AppService, KitchenService, RecommenderService],
})
export class AppModule {}
