import { WebSocketGateway, WebSocketServer, SubscribeMessage, OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';

@WebSocketGateway()
export class MessagingGateway implements OnGatewayConnection, OnGatewayDisconnect {

    @WebSocketServer() server;
    users: number = 0;

    async handleConnection(){

        // A client has connected
        this.users++;

        // Notify connected clients of current users
        this.server.emit('users', this.users);

    }

    async handleDisconnect(){

        // A client has disconnected
        this.users--;

        // Notify connected clients of current users
        this.server.emit('users', this.users);

    }

    @SubscribeMessage('send_message')
    async onChat(client, message){
        console.log(message);
        client.broadcast.emit('receive_message', message);
    }
   
    async sendMessage(body: any, topic: string){
        console.log(`Sending message to topic: ${topic} ${body}`);
        this.server.emit(topic, body);
        return {"message": "Event sent successfully"};
    }

}