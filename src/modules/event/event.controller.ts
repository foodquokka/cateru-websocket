import { Controller, Get, Post, Body } from '@nestjs/common';
import { MessagingGateway } from 'src/modules/messaging/messaging.gateway';

@Controller('/event')
export class EventController {    
    constructor(private readonly messaging: MessagingGateway){}

    @Post('/send')
    async sendRefreshEvent(@Body() body:any){
        return await this.messaging.sendMessage(body['content'], body['topic']);
    }


}

