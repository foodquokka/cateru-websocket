import { Module } from '@nestjs/common';
import { DispatcherController } from './dispatcher.controller';
import { KitchenModule } from '../kitchen/kitchen.module';
import { DbModule } from '../db/db.module';
import { DispatcherService } from './dispatcher.service';

@Module({
    imports: [KitchenModule,DbModule],
    providers: [DispatcherService],
    controllers: [DispatcherController],

})
export class DispatcherModule {}
