import { Controller, Get, Post, Body } from '@nestjs/common';
import { RecService } from './rec.service';

@Controller('rec')
export class RecController {

    constructor(private recService: RecService){}

@Get('')
async getStartData(){
    return await this.recService.getAprioriData();
}

@Post('')
async getRecommendation(@Body("items") items: string[], @Body('orderId') orderId:number){
    if (items.length == 0) {
        console.log(items);
        console.log("empty payload!");
        return {menu: []};
    }
    return await this.recService.getRecommendation(items, orderId);
}

}
